package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/awserr"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

type HealthNotice struct {
	Version    string   `json:"version"`
	ID         string   `json:"id"`
	DetailType string   `json:"detail-type"`
	Source     string   `json:"source"`
	Account    string   `json:"account"`
	Time       string   `json:"time"`
	Region     string   `json:"region"`
	Resources  []string `json:"resources"`
	Detail     Event    `json:"detail"`
}

type Event struct {
	EventARN          string `json:"eventArn"`
	Service           string `json:"service"`
	EventTypeCode     string `json:"eventTypeCode"`
	EventTypeCategory string `json:"eventTypeCategory"`
	startTime         string `json:"startTime"`
}

var tagName = "AWSHealthAction"
var wait = time.Duration(3)

func handleRequest(ctx context.Context, health HealthNotice) (string, error) {
	return doWork(health), nil
}

func main() {
	if len(os.Args) < 2 {
		lambda.Start(handleRequest)
	} else {
		filepath := fmt.Sprintf("%s", os.Args[1])
		data, err := ioutil.ReadFile(filepath)
		check(err)

		health := HealthNotice{}
		err = json.Unmarshal(data, &health)
		check(err)
		fmt.Println(doWork(health))
	}
}

func doWork(health HealthNotice) string {
	output := ""
	for _, i := range health.Resources {
		//fmt.Println("Resource:", i)
		svc := ec2.New(session.New(&aws.Config{Region: aws.String(health.Region)}))
		input := &ec2.DescribeTagsInput{
			Filters: []*ec2.Filter{
				{
					Name: aws.String("resource-id"),
					Values: []*string{
						aws.String(i),
					},
				},
			},
		}

		result, err := svc.DescribeTags(input)
		chk := checkError(err)
		if chk != "" {
			log.Println(chk)
			output = fmt.Sprintf("%s%s\n", output, chk)
			return output
		}

		action := ""

		for _, t := range result.Tags {
			//fmt.Printf("Key: %s Value: %s\n", *t.Key, *t.Value)
			if *t.Key == tagName { //}&& (*t.Value == "true") {
				action = *t.Value
				//fmt.Printf("%s with tag: %s value: %s is safe to be rebooted\n", i, *t.Key, *t.Value)
			}
		}
		switch action {
		case "terminate":
			status := checkStatus(i, svc)
			fmt.Println(i, "-", status)
			if status == "running" {
				input := &ec2.TerminateInstancesInput{
					InstanceIds: []*string{
						aws.String(i),
					},
				}

				result, err := svc.TerminateInstances(input)
				chk := checkError(err)
				if chk != "" {
					log.Println(chk)
					output = fmt.Sprintf("%s%s\n", output, chk)
					return output
				}
				output = fmt.Sprintf("%s%s\n", output, result.GoString())

				terminated := false
				counter := 0

				for ok := true; ok; ok = !terminated {
					counter += 1
					status := checkStatus(i, svc)
					if status == "terminated" {
						fmt.Println(i, "-", status)
						terminated = true
					} else {
						fmt.Println(i, "-", status, "(waiting for terminated) sleeping for", int(wait), "seconds")
						time.Sleep(wait * time.Second)
						if counter > 40 {
							fmt.Println("Counter exceeded:", counter)
							os.Exit(1)
						}
					}
				}
			}

		case "restart":
			resp := checkStatus(i, svc)
			fmt.Println(i, "-", resp)
			if resp == "running" {
				fmt.Println("Stopping -", i)
				input := &ec2.StopInstancesInput{
					InstanceIds: []*string{
						aws.String(i),
					},
				}

				_, err := svc.StopInstances(input)
				_ = checkError(err)
			}

			stopped := false
			counter := 0

			for ok := true; ok; ok = !stopped {
				counter += 1
				status := checkStatus(i, svc)
				if status == "stopped" {
					fmt.Println(i, "-", status)
					stopped = true
				} else {
					fmt.Println(i, "-", status, "(waiting for stopped) sleeping for", int(wait), "seconds")
					time.Sleep(wait * time.Second)
					if counter > 40 {
						fmt.Println("Counter exceeded:", counter)
						os.Exit(1)
					}
				}
			}

			/* Start Instance */
			fmt.Println("Starting -", i)
			startInput := &ec2.StartInstancesInput{
				InstanceIds: []*string{
					aws.String(i),
				},
			}

			_, err := svc.StartInstances(startInput)
			_ = checkError(err)

			running := false
			counter = 0

			for ok := true; ok; ok = !running {
				counter += 1
				status := checkStatus(i, svc)
				if status == "running" {
					fmt.Println(i, "-", status)
					running = true
				} else {
					fmt.Println(i, "-", status, "(waiting for running) sleeping for", int(wait), "seconds")
					time.Sleep(wait * time.Second)
					if counter > 40 {
						fmt.Println("Counter exceeded:", counter)
						os.Exit(1)
					}
				}
			}
		default:
			_ = ""
		}
	}

	if output == "" {
		output = "Nothing to do."
	}
	return output
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func checkStatus(instance string, svc *ec2.EC2) string {
	state := ""
	input := &ec2.DescribeInstancesInput{
		InstanceIds: []*string{
			aws.String(instance),
		},
	}

	result, err := svc.DescribeInstances(input)
	state = checkError(err)

	if state == "" {
		//fmt.Println(result)
		for _, r := range result.Reservations {
			for _, i := range r.Instances {
				state = *i.State.Name
			}
		}
	}
	return state
}

func checkError(err error) string {
	state := ""
	if err != nil {
		if aerr, ok := err.(awserr.Error); ok {
			switch aerr.Code() {
			default:
				fmt.Println(aerr.Error())
				state = fmt.Sprintf("%s", aerr.Error())
			}
		} else {
			// Print the error, cast err to awserr.Error to get the Code and
			// Message from an error.
			fmt.Println(err.Error())
			state = fmt.Sprintf("%s", err.Error())
		}
	}
	return state

}

/*
{
  "version": "0",
  "id": "7fb65329-1628-4cf3-a740-95fg457h1402",
  "detail-type": "AWS Health Event",
  "source": "aws.health",
  "account": "123456789101",
  "time": "2016-06-05T06:27:57Z",
  "region": "us-east-1",
  "resources": ["i-12345678"],
  "detail": {
    "eventArn": "arn:aws:health:region::event/id",
    "service": "EC2",
    "eventTypeCode": "AWS_EC2_DEDICATED_HOST_NETWORK_MAINTENANCE_SCHEDULED",
    "eventTypeCategory": "scheduledChange",
    "startTime": "Sat, 05 Jun 2016 15:10:09 GMT",
    "eventDescription": [{
      "language": "en_US",
      "latestDescription": "A description of the event will be provided here"
    }],
    "affectedEntities": [{
      "entityValue": "i-12345678",
      "tags": {
        "stage": "prod",
        "app": "my-app"
      }
    }]
  }
}
*/
